<?php

namespace Drupal\cloudfront_cache_path_invalidate\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;

/**
 * Configure Automatic Cloudfront Cache settings for this site.
 */
class AutoCloudfrontCacheSettingForm extends ConfigFormBase {
  use StringTranslationTrait;

  /**
   * Drupal\Core\Entity\EntityTypeManagerInterface definition.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * Config settings.
   *
   * @var string
   */
  const SETTINGS = 'cloudfront_cache_path_invalidate.settings';

  /**
   * Constructs a new Cloudfront Cache clear form object.
   */
  public function __construct(
    EntityTypeManagerInterface $entityTypeManager
  ) {
    $this->entityTypeManager = $entityTypeManager;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('entity_type.manager')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'cloudfront_cache_path_invalidate_settings_form';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return [
      self::SETTINGS,
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $values = $this->config(self::SETTINGS);
    // Gather the number of names in the form already.
    $group_counts = $form_state->get('ecgroupcounttemp');

    if ($group_counts === NULL) {
      $current_count = $values->get('ecgroupcount') ? $values->get('ecgroupcount') : 1;
      $form_state->set('ecgroupcounttemp', $current_count);
      $group_counts = $current_count;
    }

    $form['#tree'] = TRUE;
    $form['ec_fieldset'] = [
      '#type' => 'fieldset',
      '#title' => $this->t('Automatic Cloudfront Cache Path Invalidate Settings for hook_enity_*()'),
      '#prefix' => '<div id="names-fieldset-wrapper">',
      '#suffix' => '</div>',
    ];

    $entity_options = [];

    foreach ($this->entityTypeManager->getDefinitions() as $entity_type) {
      if ($entity_type->getBundleEntityType() === NULL) {
        $entity_options[$entity_type->id()] = $entity_type->getLabel();
      }
    }

    asort($entity_options);
    array_unshift($entity_options, $this->t('--Select--'));

    for ($i = 0; $i < $group_counts; $i++) {
      $selected_entity_type = $form_state->getValue(['ec_fieldset', $i, 'ecentitytype'], $values->get('ecentitytype')[$i] ?? key($entity_options));
      $form['ec_fieldset'][$i]['ecentitytype'] = [
        '#type' => 'select',
        '#title' => $this->t('Entity Type'),
        '#options' => $entity_options,
        '#required' => TRUE,
        '#weight' => -1,
        '#default_value' => $selected_entity_type,
        '#ajax' => [
          'callback' => '::entityTypeBundle',
          'wrapper' => "entity-bundle-container_$i",
        ],
      ];

      $entity_bundle_options = [];

      if (!empty($selected_entity_type)) {
        foreach ($this->entityTypeManager->getStorage($selected_entity_type)->loadMultiple() as $entity_bundle) {
          $entity_bundle_options[$entity_bundle->id()] = $entity_bundle->label();
        }
      }

      asort($entity_bundle_options);
      array_unshift($entity_bundle_options, $this->t('--Select--'));

      $form['ec_fieldset'][$i]['entity_bundle_container'] = [
        '#type' => 'container',
        '#attributes' => ['id' => "entity-bundle-container_$i"],
      ];

      $form['ec_fieldset'][$i]['entity_bundle_container']['ecentitytypebundle'] = [
        '#type' => 'select',
        '#title' => $this->t('Entity Bundle'),
        '#options' => $entity_bundle_options,
        '#required' => TRUE,
        '#weight' => 2,
        '#default_value' => $form_state->getValue(['ec_fieldset', $i, 'ecentitytypebundle'], $values->get('ecentitytypebundle')[$i] ?? ''),
      ];

      $form['ec_fieldset'][$i]['detail_page'] = [
        '#type' => 'checkbox',
        '#title' => $this->t('Detail Page'),
        '#default_value' => $form_state->getValue(['ec_fieldset', $i, 'detail_page'], $values->get('detail_page')[$i] ?? FALSE),
        '#weight' => 3,
      ];

      $form['ec_fieldset'][$i]['ec_cloudfront_url'] = [
        '#type' => 'textarea',
        '#title' => $this->t('Cloudfront URL to invalidate'),
        '#description' => $this->t('Specify the existing path you wish to invalidate. For example: /sector/*, /state/*. Enter one value per line.'),
        '#placeholder' => $this->t('Cloudfront URL'),
        '#weight' => 4,
        '#default_value' => $form_state->getValue(['ec_fieldset', $i, 'ec_cloudfront_url'], $values->get('ec_cloudfront_url')[$i] ?? ''),
      ];
    }

    $form['ec_fieldset']['actions'] = [
      '#type' => 'actions',
    ];
    $form['ec_fieldset']['actions']['add_more_ec'] = [
      '#type' => 'submit',
      '#value' => $this->t('Add more'),
      '#submit' => ['::addMore'],
      '#ajax' => [
        'callback' => '::addMoreCallback',
        'wrapper' => ['names-fieldset-wrapper'],
      ],
    ];

    if ($group_counts >= 1) {
      $form['ec_fieldset']['actions']['remove_name'] = [
        '#type' => 'submit',
        '#value' => $this->t('Remove'),
        '#submit' => ['::removeCallback'],
        '#limit_validation_errors' => [],
        '#ajax' => [
          'callback' => '::addMoreCallback',
          'wrapper' => ['names-fieldset-wrapper'],
        ],
      ];
    }

    $form['actions']['submit'] = [
      '#type' => 'submit',
      '#value' => $this->t('Submit'),
    ];

    return $form;
  }

  /**
   * Callback for both ajax-enabled buttons.
   *
   * Selects and returns the fieldset with the names in it.
   */
  public function addMoreCallback(array &$form, FormStateInterface $form_state) {
    return $form['ec_fieldset'];
  }

  /**
   * Callback for both ajax-enabled buttons.
   *
   * Selects and returns the entity bundle container.
   */
  public function entityTypeBundle(array &$form, FormStateInterface $form_state) {
    $triggered_element = $form_state->getTriggeringElement();
    $index = $triggered_element['#parents'][1];
    return $form['ec_fieldset'][$index]['entity_bundle_container'];
  }

  /**
   * Submit handler for the "add-one-more" button.
   *
   * Increments the max counter and causes a rebuild.
   */
  public function addMore(array &$form, FormStateInterface $form_state) {
    $add_button = $form_state->get('ecgroupcounttemp') + 1;
    $form_state->set('ecgroupcounttemp', $add_button);
    $form_state->setRebuild();
  }

  /**
   * Submit handler for the "remove one" button.
   *
   * Decrements the max counter and causes a form rebuild.
   */
  public function removeCallback(array &$form, FormStateInterface $form_state) {
    $name_field = $form_state->get('ecgroupcounttemp');

    if ($name_field >= 1) {
      $remove_button = $name_field - 1;
      $form_state->set('ecgroupcounttemp', $remove_button);
    }

    // Since our buildForm() method relies on the value of 'num_names' to
    // generate 'name' form elements, we have to tell the form to rebuild. If we
    // don't do this, the form builder will not call buildForm().
    $form_state->setRebuild();
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    $fieldset_values = $form_state->getValue(['ec_fieldset']);
  
    foreach ($fieldset_values as $key => $value) {

      if (!empty($value['ec_cloudfront_url'])) {
        $url_value = explode("\n", $value['ec_cloudfront_url']);
        foreach ($url_value as $url) {
          if (trim($url) !== '' && substr($url, 0, 1) !== '/') {
            $form_state->setErrorByName("ec_fieldset][$key][ec_cloudfront_url", $this->t('The Cloudfront URL is not valid.'));
            break;
          }
        }
      }
    }
    
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $values = $form_state->getValue(['ec_fieldset']);

    $ecentitytype = [];
    $ecentitytypebundle = [];
    $detail_page = [];
    $ec_cloudfront_url = [];

    foreach ($values as $value) {
      $ecentitytype[] = $value['ecentitytype'] ?? '';
      $ecentitytypebundle[] = $value['entity_bundle_container']['ecentitytypebundle'] ?? '';
      $detail_page[] = $value['detail_page'] ?? '';
      $ec_cloudfront_url[] = $value['ec_cloudfront_url'] ?? '';
    }

    $this->config(self::SETTINGS)
      ->set('ecentitytype', $ecentitytype)
      ->set('ecentitytypebundle', $ecentitytypebundle)
      ->set('detail_page', $detail_page)
      ->set('ec_cloudfront_url', $ec_cloudfront_url)
      ->set('ecgroupcount', count($values))
      ->save();
    $this->messenger()->addStatus($this->t('Cloudfront Cache Setting has been saved.'));
    
  }

}