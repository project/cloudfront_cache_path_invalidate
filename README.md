
# CloudFront Cache Path Invalidate

Cloudfront Cache Path Invalidate module, you can manage the cache clear of
Amazon Cloudfront through a setting form.

For a full description of the module, visit the
[project page](https://www.drupal.org/project/cloudfront_cache_path_invalidate).

Submit bug reports and feature suggestions, or track changes in the
[issue queue](https://www.drupal.org/project/issues/cloudfront_cache_path_invalidate).

## Table of contents

- Requirements
- Installation
- Configuration
- Maintainers

## Requirements

This module requires the following :
- Drupal 10.x
- CloudFront Setup
    AWS Distribution ID
    AWS Access Key
    AWS Secret Key
    AWS Region

## Installation

Install the CloudFront Cache Path Invalidate:
Download it from [CloudFront Cache Path Invalidate](https://www.drupal.org/project/cloudfront_cache_path_invalidate) and install it on your website.

- With Composer
  `$ composer require 'drupal/cloudfront_cache_path_invalidate'`

## Configuration

To configure the module to invaliate desired paths in Cloudfront, we weill need to provide AWS credentials and configure which entity types to trigger invalidations.

### 1. AWS credentials

The module needs the following AWS credentials to be configured as part of the $settings array in settings.php or settings.local.php.


```
$settings['aws.distributionid'] = ''; // e.g. 'ABCD1234EFGH7'
$settings['aws.region'] = ''; // e.g. 'eu-west-2'
$settings['aws.access_key'] = ''; // e.g. 'AKIAIOSFODNN7EXAMPLE'
$settings['aws.secret_key'] = ''; // e.g. 'wJalrXUtnFEMI/K7MDENG/bPxRfiCYEXAMPLEKEY'
```

You will need to generate these from your AWS Identity and Access Management (IAM) control panel. 

For more information on managing IAM users and keys, see https://docs.aws.amazon.com/IAM/latest/UserGuide/id_credentials_access-keys.html

### 2. Configuring behaviour

Go to /admin/config/services/auto-cloudfront-cache-entities and select the entity type and bundle you want to trigger an invalidation. 

For example, for a 'Article' content type:  

 - For "Entity type" select "Content type"
 - for "Entity Bundle" select "Article"
 - Toggle "Detail page"

If you also wish to invalidate additional paths when the "Page" content type is updated, you can add thess to the "Cloudfront URL to invalidate" field. For example, you might want a views listing at /articles to be invalidated when you update one of your articles.

Click "Submit.

### 3. Test expected behaviour 

When you now edit and save a "Page' node, the module will attempt to connect to AWS Cloudfront and invalidate the canonical path and the path alias of the edited node.

### 3. Check the databse logs

After configuring the module, try editing and saving content to trigger your cache clearing rules. If successful, we should see something like this in the recent log messgaes: 

```
Cloudfront Auto Cache Flush on Update entity_type: node_type bundle_type: article bundle: node entity_id: 123 and paths /articles,/articles/my-latest-updated-article
```


## Maintainers
- Prashant Mishra - [PrashantMishra](https://www.drupal.org/u/prashant-mishra)